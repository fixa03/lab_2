﻿using System;
using System.Collections.Generic;

namespace lab_2
{
    class Program
    {

        static string[] DaysName = {
        "Пн",
        "Вт",
        "Ср",
        "Чт",
        "Пт",
        "Сб",
        "Вс",
        };

        static string[] MonthName = {
        "Январь",
        "Февраль",
        "Март",
        "Апрель",
        "Май",
        "Июнь",
        "Июль",
        "Август",
        "Сентябрь",
        "Октябрь",
        "Ноябрь",
        "Декабрь",
        };

        static bool IsYearMod4(int year)
        {
            if (year % 4 == 0)
            {
                if ((year % 100 != 0) || (year % 400 == 0))
                    return true;
            }

            return false;


        }

        static int FirstDayInYear(int year)
        {
            int init_year = 1901;
            int init_day = 1;

            for (int i = init_year + 1; i <= year; i++)
            {
                init_day = (init_day + 1) % 7;
                if (IsYearMod4(i - 1)) init_day = (init_day + 1) % 7;

            }
            return init_day;

        }

        static int DaysInMonth(int month, int year)
        {

            int February = IsYearMod4(year) ? 29 : 28;
            int[] days = { 31, February, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

            return days[month];
        }

        static int FirstDayInMonth(int month, int year)
        {

            int day = FirstDayInYear(year);

            if (month == 0)
                return day;

            for (int i = 0; i != month; i++)
            {
                for (int j = 0; j < DaysInMonth(i, year); j++)
                {
                    day = (day + 1) % 7;

                }

            }

            //day = (day + 1) % 7;
            return day;



        }


        static void Main(string[] args)
        {

            int year = Convert.ToInt32(Console.ReadLine());
            int month = Convert.ToInt32(Console.ReadLine()) - 1;


            Console.WriteLine('\t' + year.ToString());
            Console.WriteLine('\t' + MonthName[month]);

            //char step;

            int first_day = FirstDayInMonth(month, year);
            int this_day = first_day;
            int[,] days_arrays = new int[DaysName.Length, 5];
            //bool flag_start = true;


            for (int day = 0; day < DaysName.Length; day++)
            {
                for (int i = 0; i < 5; i++)
                {
                    if (i == 0)
                    {
                        if (day == first_day)
                        {
                            days_arrays[first_day, 0] = 1;
                        }
                        else
                        {
                            if (day < first_day)
                            {
                                int cols = 6 - first_day;
                                days_arrays[day, 0] = ((6 - first_day) + 1) + 1 + day; //day + ((first_day + 1) % 7) + (7 - (first_day + 1)) + 2;
                            }
                            else
                            {
                                days_arrays[day, 0] = 1 + (day - first_day);


                            }
                        }
                    }
                    else
                    {

                        if ((days_arrays[day, i - 1] + 7) <= DaysInMonth(month, year))
                            days_arrays[day, i] = days_arrays[day, i - 1] + 7;

                    }

                }



                Console.Write(DaysName[day] + '\t');
                if (day < first_day)
                    Console.Write("  ");
                for (int j = 0; j < 5; j++)
                {
                    if (days_arrays[day, j] != 0)
                        Console.Write(days_arrays[day, j].ToString() + ' ');
                }
                Console.WriteLine();

            }

            ;
            //for (int day = 0; day < DaysName.Length; day++)
            //{

            //    while (this_day == day)
            //    {
            //        for (int number = 1, i = 0; number < DaysInMonth(month, year); number++)
            //        {
            //            if (this_day == day)
            //            {
            //                days_arrays[day, i] = number;
            //                i++;
            //            }


            //        }
            //        this_day = (this_day + 1) % 7;
            //    }

            //}

            ;

            //for (int day = 0; day < DaysName.Length; day++)
            //{
            //    Console.Write(DaysName[day]);


            //    if (first_day == day)
            //        Console.Write("  1");

            //    this_day = (first_day + this_day + 1) % 7;    
            //    for (int number = 2; number <= DaysInMonth(month, year); number++)
            //    {
            //        if (this_day == day && flag_start && this_day != first_day)
            //        {

            //            if (day < first_day)
            //                Console.Write("  \t" + number.ToString());

            //            if (day > first_day)
            //                Console.Write("  " + number.ToString());

            //            //if (day == first_day)
            //            //    Console.Write("/t" + number.ToString());

            //            flag_start = false;
            //        }
            //        else
            //        {
            //            if (this_day == day)
            //                Console.Write('\t' + number.ToString());

            //        }

            //        this_day = (this_day + 1) % 7;

            //    }
            //    flag_start = true;
            //    Console.WriteLine();

            //}

            //https://calendum.ru/2000#%D0%9A%D0%B0%D0%BB%D0%B5%D0%BD%D0%B4%D0%B0%D1%80%D1%8C-2000
            //https://allcalc.ru/node/359


            //while (true) {

            //    //WeekDays start_day = init_day;
            //    int s = year + 10;
            //    do
            //    {
            //        for (int i = init_year; i < year; i++)
            //        {
            //            init_day++;
            //            if (IsYearMod4(i)) init_day++;
            //            if ((init_day) > 6) init_day = 0;

            //        }

            //        Console.WriteLine(year + " " + DaysName[(init_day)] + " " +init_day );
            //        year += 1;
            //    } while (year < s);
            //    year = Convert.ToInt32(Console.ReadLine());
            //    Console.Clear();
            //}
        }
    }
}